﻿///using file handling

using Day5.Repository;
using Day5.Model;
using Day5.Exceptions;
using System.IO;

UserRepository userRepository = new UserRepository();
IUserRepository iuserRepository = (IUserRepository) userRepository;
User user = new User();
Console.WriteLine("Welcome to registration ");
Console.WriteLine("ID: ");
user.Id = int.Parse(Console.ReadLine());
Console.WriteLine("Name: ");
user.Name = Console.ReadLine();
Console.WriteLine("City: ");
user.City = (Console.ReadLine());

//bool isUserRegistered = iuserRepository.RegisterUser(new User() { Id=1, Name="user1",City="Mumbai" });
//bool isUserRegistered = iuserRepository.RegisterUser(user);
//if (isUserRegistered) { 
//    Console.WriteLine("Registered Successfuly");
//}
//else
//{
//    Console.WriteLine("Username exists already");
//}


try
{
    iuserRepository.RegisterUser(user);
}
catch(InvalidUser ex)
{
    Console.WriteLine(ex.Message);
}
Console.WriteLine("Reading Conents from file");
List<string> userContent = userRepository.ReadContentsFromFile("userDatabase.txt");
foreach(string filedata in userContent)
{
    Console.WriteLine(filedata);
}
foreach(string files in userContent)
{
    
}
// isUserRegistered = iuserRepository.RegisterUser(new User() { Id = 2, Name = "user2", City = "Pune" });

//if (isUserRegistered)
//{
//    Console.WriteLine("Registered Successfuly");
//}
//else
//{
//    Console.WriteLine("Registeration Unsuccessful");
//}
Console.ReadLine();    