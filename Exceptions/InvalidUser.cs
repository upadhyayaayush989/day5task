﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Exceptions
{
    internal class InvalidUser:Exception
    {
        public InvalidUser()
        {

        }
        public InvalidUser(string message) : base(message)
        {

        }
    }
}
